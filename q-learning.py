#Q Learning example
#reimplementation of http://mnemstudio.org/path-finding-q-learning-tutorial.htm
import numpy as np
import pandas

class FixedStateQLearner:
    def __init__(self):
        self.qSize = 6
        self.gamma = 0.8
        self.iterations = 10
        self.initial_states = (1,3,5,2,4,0)
        #get to the goal and stay there
        self.goalState = 5
        self.currentState = 0

        #rewards matrix
        #-1 indicates "you can't get there from here"
        #0 indicates a valid transition
        #100 indicates reaching the goal state
        self.R = pandas.DataFrame([
          [-1, -1, -1, -1, 0, -1],
          [-1, -1, -1, 0, -1, 100],
          [-1, -1, -1, 0, -1, -1],
          [-1, 0, 0, -1, 0, -1],
          [0, -1, -1, 0, -1, 100],
          [-1, 0, -1, -1, 0, 100],
        ])
        #Q matrix
        self.Q = pandas.DataFrame( np.zeros((self.qSize,self.qSize)) )

    def train(self):
        #train thru all initial states
        for epoch in range(self.iterations):
            for i in range(self.qSize):
                self.episode(self.initial_states[i])
        print("Q is now ")
        print(self.Q)

    def test(self):
        print('looking for shortest route')
        for i in range(self.qSize):
            self.currentState = self.initial_states[i]
            path = [self.initial_states[i], ]

            newState = 0
            while self.currentState < self.goalState:
                newState = self.qmax(self.currentState, True)
                print('Current state:', self.currentState)
                path.append(newState)
                self.currentState = newState
            print(path)

    def episode(self, initialState):
        #track the number of steps taken to reach the goal and penalize more steps
        totalSteps = 0
        self.currentState = initialState
        print('Starting a new episode')
        #play a round looking for solution
        while self.currentState != 5:
            self.chooseAction()
            print('currentState is ', self.currentState)
        for i in range(self.qSize):
            self.chooseAction() #do again for convergence?

    def chooseAction(self):
        #randomly choose a valid action
        #NOTE: in this model the action = next state after transition
        possibleAction = self.getRandomAction()
        print('possible action', possibleAction, ', R value is ', self.R[possibleAction][self.currentState])
        #ensure its a valide move since invalid moves are <0
        if self.R[possibleAction][self.currentState] >= 0:
            self.Q[possibleAction][self.currentState] = self.reward(possibleAction)
            self.currentState = possibleAction

    def getRandomAction(self, ):
        validActions = list(self.R[self.R.loc[self.currentState,:]>=0].index)
        print('validActions for ', self.currentState,' are ', validActions)
        #
        return validActions[ np.random.randint(0, len(validActions)) ]

    def qmax(self, nextstate, returnIndex):
        #return the maximum of the next state and all actions in that state
        #returnIndex controls whether the index or value is returned
        index = self.Q.loc[nextstate,:].argmax()
        print('Max is %s at index %s' % (self.Q.loc[nextstate,index], index))
        if returnIndex:
            return index
        else:
            return self.Q.loc[nextstate,index]

    def reward(self, actionsNextState):
        return self.R[actionsNextState][self.currentState] + self.gamma * self.qmax(actionsNextState, False)

if __name__ == "__main__":
    wf = FixedStateQLearner()
    wf.train()
    import pdb; pdb.set_trace()
    wf.test()
