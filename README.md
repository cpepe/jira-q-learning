# JIRA Workflows for lazy users
The intent of this project is to use Q learning to minimize the number of clicks needed to take a JIRA issue from a given state to closed. It pays no attention to the value of following the happy path, reporting up to management, or gathering metrics. It seeks only to get to the letter of done while ignoring the spirit of the definition of done.

See [jira-q-learning.py](jira-q-learning.py)

# Requirements
The requirements are scant and versions shouldn't matter too much. Development was done with the stock python 2.7 on OSX. The versions I happen to have are listed.

* numpy 1.8.0rc1
* pandas 0.17.0

# Data
This representation of a JIRA workflow expresses the cost of moving a JIRA issue by the number of clicks required to execute a transition (independent of the # of strokes to populate the fields). We naively assume that every field will be filled out and that they require the same effort. Thus the cost is

    # fields on transition screen + 1 for comment + 1 for Submit

For example to transition from _Open_ to _Resolved_ the _Resolve Issue_ transition is used. It has a screen with 4 fields and thus has a cost of 6. _Open_ to _In Progress_ is immediate with no transition screen and so its cost is 0.

pcm-production-wf-cost.csv sums up a marketing workflow from a real JIRA instance. This waterfall-esque workflow has a long, one-way happy path, and a few parking statuses. Every transition has a screen with 3 values, not that interesting but how it is.

![PCM Workflow](pcm-production-wf.png)

I have represented this workflow as a [state action matrix](data/pcm-production-wf-cost.csv) listing the cost of executing a transition in approximate number of clicks. During initialization I scale the costs to rewards, however, I assume that I could use the data as is and tune the Q to minimize cost instead of maximize reward.

The Q Learner found a great way to cheat the system. It ends up being cheaper to transition thru _On Hold_ to get to _Closed_ because _On Hold_ is a global transition, and can return to anywhere. (I believe the JIRA logic only allows one to return from where they came and that is not accounted for in this model.)

Here we find the optimal path for an issue in a given status:

    INFO:root:['Open', 'On Hold', 'Published', 'Closed']
    INFO:root:['Reopened', 'On Hold', 'Published', 'Closed']
    INFO:root:['In Progress', 'On Hold', 'Published', 'Closed']
    INFO:root:['In QA', 'On Hold', 'Published', 'Closed']
    INFO:root:['Pending Publishing', 'In Final QA', 'Published', 'Closed']
    INFO:root:['In Final QA', 'Published', 'Closed']
    INFO:root:['Published', 'Closed']
    INFO:root:['On Hold', 'Published', 'Closed']
    INFO:root:['Blocked', 'Published', 'Closed']
    INFO:root:['Closed']

Note that in most cases we jump immediately to _On Hold_ to get to _Published_ and then _Closed_. This model misses a shorter path from _In QA_ directly to _Closed_ via the _Cancel_ transition. That is because the arbitrary reward that I selected, 5, is not high enough and/or I did not train long enough. This will be an area to learn more.

          Set Blocked  Close  Cancel  
    In QA      6.4       0      5  

This is surmounted by using an additional episode playing pattern suggested in the source article. I am still noodling thru the value it provides. See the last few lines of episode().

    INFO:root:['In QA', 'Closed']

                   Set Blocked      Close     Cancel  
    In QA           13.114753      0.000000  15.491803

# Next Steps
There are a number of naive assumptions in this model. Replaying issues (via JIRA issue history) to see how many keystrokes and transitions people really made would better inform the cost of a transition. Also ingesting a workflow XML to create the cost CSV would make this tool more generic.
