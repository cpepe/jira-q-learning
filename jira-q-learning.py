#Q Learning JIRA Workflows
#based on http://mnemstudio.org/path-finding-q-learning-tutorial.htm
import numpy as np
import pandas
import logging
logging.basicConfig(level=logging.INFO)
import pdb

class GreedyQLearner:
    def __init__(self):
        #get to the goal and stay there
        self.goalState = 'Closed'
        self.currentState = 'Open'
        self.goalTransitions = ()
        self.action2status = dict()
        self.R = None
        self.qSize = (0,0)
        self.gamma = 0.8
        self.iterations = 10
        self.initial_states = list()

    def loadCostCSV(self, filepath):
        #rewards matrix
        #load in the JIRA workflow costs, invert costs to get lower cost = higher reward, label 'no-transition' as -1
        self.R = pandas.read_csv(filepath, index_col=0)
        maxCost = max(list( self.R.max(numeric_only=True) ))
        self.R = (maxCost-self.R)/maxCost
        #skew the reward for reaching the goal state to ensure that transition is the max reward
        #here again we could infer this from the data file. The intent is to make completing the issue more rewarding
        # but allow canceling to be a valid path.
        for action in self.goalTransitions:
            if action == 'Cancel':
                self.R[action] = self.R[action]+5
            else:
                self.R[action] = self.R[action]+10
        self.R = self.R.fillna(-1)
        self.qSize = (len( self.R.index ), len(self.R.columns)) #assuming Q and R have same dimensions
        self.initial_states = list(self.R.index)
        #Q matrix
        self.Q = pandas.DataFrame( np.zeros(self.qSize), index=self.R.index, columns=self.R.columns )

    def train(self):
        logging.info('R is \n%s' % (self.R))
        logging.info("Q starts as \n%s" % (self.Q))
        #train thru all initial states
        for epoch in range(self.iterations):
            log.info('Training epoch %s' % (epoch))
            for state in self.initial_states:
                self.episode(state)
        logging.info('R is \n%s' % (self.R))
        logging.info("Q became \n%s" % (self.Q))

    def test(self):
        logging.debug('looking for shortest route')
        logging.info('----------------------------------')
        logging.info('Path to Closed for a given Status:')
        for state in self.initial_states:
            self.currentState = state
            path = [state, ] #kinda an ugly way to init a 1 element list
            newState = 0
            while self.currentState != self.goalState:
                newState = self.action2status[ self.qmax(self.currentState, True) ]
                logging.debug('Current state: %s' % (self.currentState))
                path.append(newState)
                self.currentState = newState
            logging.info(path)

    def episode(self, initialState):
        self.currentState = initialState
        logging.debug('------- Starting a new episode ---------')
        #play a round looking for solution
        while self.currentState != self.goalState:
            try:
                self.chooseAction()
                logging.debug('[train] currentState is %s' % (self.currentState))
            except:
                pdb.set_trace()
        #Enabling this block allows the learner to figure out that from In QA one can short circuit to closed via the Cancel transition. I don't fully follow what this is doing beyond running more running around the map.
        #WARN: This would FAIL in the event of a terminal state (a status that you can't transition out of)
        for i in range(self.qSize[0]):
            logging.debug('Current State is %s' % (self.currentState))
            self.chooseAction() #do again to reinforce learning

    def chooseAction(self):
        #randomly choose a valid action
        #wow, let's unpack this:
        # cs = self.currentState - Get the row of transitions for the status we are in now
        # transBool = self.R.loc[self.currentState,:]>=0 - T/F transition is available for this status if it >= 0
        # self.R.loc[cs, transBool].index.tolist() - make a list of valid transitions for this status
        validActions = self.R.loc[self.currentState, self.R.loc[self.currentState,:]>=0].index.tolist()
        logging.debug('validActions for %s are %s' % (self.currentState, validActions))
        possibleAction = validActions[ np.random.randint(0, len(validActions)) ]
        logging.debug('possible action %s, R value is %s' % (possibleAction, self.R[possibleAction][self.currentState]))
        #ensure its a valide move since invalid moves are <0
        if self.R[possibleAction][self.currentState] >= 0:
            self.Q[possibleAction][self.currentState] = self.reward(possibleAction)
            self.currentState = self.action2status[possibleAction]

    def qmax(self, state, returnIndex):
        try:
            #return the maximum of the next state and all actions in that state
            #returnIndex controls whether the index or value is returned
            index = self.Q.loc[state,:].argmax()
            logging.debug('Max is %s at index %s' % (self.Q.loc[state,index], index))
            if returnIndex:
                return index
            else:
                return self.Q.loc[state,index]
        except:
            pdb.set_trace()

    def reward(self, action):
        nextstate = self.action2status[action]
        return self.R[action][self.currentState] + self.gamma * self.qmax(nextstate, False)

if __name__ == "__main__":
    wf = GreedyQLearner()
    #Here it would be better to encode the transitions and status into the cost map by adding a 3rd dimension
    #but it was faster to do this way for the PoC.

    #PCM workflow
    wf.goalTransitions = ('Cancel', 'Close') #this should also be inferred from the data file
    wf.action2status = {'Start QA': 'In QA', 'Publish Pending': 'Pending Publishing', 'Start Progress': 'In Progress', 'Publish': 'Published', 'Start Final QA': 'In Final QA', 'Reopen': 'Reopened', 'Set Blocked': 'Blocked', 'Stop Progress': 'Open', 'Cancel': 'Closed', 'Close': 'Closed', 'Hold': 'On Hold'}
    wf.loadCostCSV('data/pcm-production-wf-cost.csv')

    #Slightly modified default JIRA Workflow
    # wf.goalTransitions = ('Closed',)
    # wf.action2status = dict(zip(wf.initial_states, wf.initial_states))
    # wf.loadCostCSV('data/jira-wf-transition-cost.csv')

    wf.train()
    wf.test()
